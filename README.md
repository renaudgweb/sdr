# SDR

Web interface to control differents CLI softwares

### Prerequisites

```
PHP server ( https://www.raspberrypi.org/documentation/remote-access/web-server/apache.md )
git
libsndfile1-dev
```

### Installing


```
sudo apt-get update && sudo apt-get install libsndfile1-dev git -y

git clone https://framagit.org/renaudgweb/sdr.git /var/www/html

```

#### WARNING: Never transmit on antenna without a band-pass filter !

## CREDIT

* https://github.com/F5OEO/rpitx

* https://github.com/ChristopheJacquet/PiFmRds

* https://github.com/twbs/bootstrap
