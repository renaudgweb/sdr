<?php
include 'strings.php';

$directory_FM = "remote/PiFmRds/src";
$directory_music = "PiFmRds/audio";

$var = "<?php\n\$frequency = ".var_export($frequency,true).";\n\$ps = ".var_export($ps,true).";\n\$rt = ".var_export($rt,true).";";

if(($_SERVER["REQUEST_METHOD"] == "POST")){
	if (isset($_POST['upload'])){
		$uploaddir = $directory_music . '/';
		$uploadfile = $uploaddir . str_replace(" ","",$_FILES['ficheiro']['name']);
		if (move_uploaded_file($_FILES['ficheiro']['tmp_name'], $uploadfile)){
			echo '<br><p style="text-align:center">File uploaded</p>';
		} else {
			echo '<br><p style="text-align:center">Oops... File not uploaded</p>';
		}

	} elseif (isset($_POST['config'])){
		$frequency = var_export($_POST["frequency"],true);
		$ps = var_export($_POST["ps"],true);
		$rt = var_export($_POST["rt"],true);
		$var = "<?php\n\n\$frequency = $frequency;\n\$ps = $ps;\n\$rt = $rt;\n\n?>";
		file_put_contents('strings.php', $var);
		header("Refresh:1");

	} elseif (isset($_POST['remove'])){
		  $directory_files = $directory_music . '/' . $_POST['remove'] . '.mp3';
			unlink($directory_files);

	} elseif (isset($_POST['play'])){
		$music = $directory_music . '/' . $_POST['play'] . '.mp3';
		$played = $var . "\n\$playing = 'yes';\n?>";
		file_put_contents('strings.php', $played);
		if($playing = 'yes'){
			shell_exec("sudo -S pkill -x pi_fm_rds");
			sleep(2);
			$out = shell_exec("sox -t mp3 /var/www/html/remote/" . $music . " -t wav -  | sudo -S /var/www/html/" . $directory_FM . "/./pi_fm_rds -freq " . $frequency . " -audio - -ps '" . $ps . "' -rt '" . $rt . "'");
			echo '<pre style="text-align:center">' . $out . '</pre>';
			echo '<br><p style="text-align:center">'.$_POST['play'] . ' played on ' . $frequency . '</p>';

		} else {
				$out = shell_exec("sox -t mp3 /var/www/html/remote/" . $music . " -t wav -  | sudo -S /var/www/html/" . $directory_FM . "/./pi_fm_rds -freq " . $frequency . " -audio - -ps '" . $ps . "' -rt '" . $rt . "'");
				echo '<pre style="text-align:center>' . $out . '</pre>';
				echo '<br><p style="text-align:center">'.$_POST['play'] . ' played on ' . $frequency . '</p>';
		}

	} elseif (isset($_POST['aux'])){
			$played = $var . "\n\$playing = 'yes';\n?>";
			file_put_contents('strings.php', $played);
			if($playing = 'yes'){
				shell_exec("sudo -S pkill -x pi_fm_rds");
				sleep(2);
				$out = shell_exec("sudo -S arecord -fs16_LE -r 44100 -Dplughw:1,0 -c 2 -  | sudo -S /var/www/html/" . $directory_FM . "/./pi_fm_rds -freq " . $frequency . " -audio - -ps '" . $ps . "' -rt '" . $rt . "'");
				echo '<pre style="text-align:center>' . $out . '</pre>';
				echo '<br><p style="text-align:center">Auxiliary channel played on ' . $frequency . '</p>';

			} else {
					$out = shell_exec("sudo -S arecord -fs16_LE -r 44100 -Dplughw:1,0 -c 2 -  | sudo -S /var/www/html/" . $directory_FM . "/./pi_fm_rds -freq " . $frequency . " -audio - -ps '" . $ps . "' -rt '" . $rt . "'");
					echo '<pre style="text-align:center>' . $out . '</pre>';
					echo '<br><p style="text-align:center">Auxiliary channel played on ' . $frequency . '</p>';
				}

	} elseif (isset($_POST['stop'])){
			shell_exec("sudo -S pkill -x pi_fm_rds");
			$stopped = $var . "\n\$playing = 'no';\n?>";
			file_put_contents('strings.php', $stopped);
			echo '<br><p style="text-align:center">Stopped.</p>';

	} elseif (isset($_POST['init'])){
			$out = shell_exec("sudo -S ./script.sh");
			echo '<pre>' . $out . '</pre>';
		}
}
?>

<!DOCTYPE HTML>
<html lang="fr">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/png" href="https://www.renaudgajda.com/favicon.ico">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Stalinist+One&family=Work+Sans:wght@300&display=swap" rel="stylesheet">
  <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
  <!-- <link rel="stylesheet" type="text/css" href="../contents/css/smartphone.css"> -->
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<title>Remote FM-RDS</title>

</head>
<body style="background-color: #bbb;">
<br>
<center>
<div style="align-content: center; min-width:300px; max-width: 800px;" class="container">
	<div class="table-responsive">
		<table style="overflow-x:auto;" class="table table-hover">
		  <thead>
			<tr>
			  <th scope="col">Song</th>
			  <th scope="col"></th>
			</tr>
		  </thead>
		  <tbody>
			<?php
				$musics = scandir($directory_music);
				unset($musics[0]); unset($musics[1]);
				foreach($musics as $files){
					echo '<tr>';
					echo '<td>' . str_replace(".mp3", "", $files) . '</td>';
					echo '<td style="width: 230px; align-content: center;">
							<form action="" method="POST">
								  <button type="submit" name="remove" class="btn btn-danger" value="' . str_replace(".mp3", "", $files) . '">Remove</button>
								  <button type="submit" name="play" class="btn btn-success" value="' . str_replace(".mp3", "", $files) . '">Play</button>
							</form>
						</td>';
					echo '</tr>';
				}
			?>
		  </tbody>
		</table>
		<form action="" method="POST">
			<button type="submit" name="init" class="btn btn-warning" value="init">Init.</button>
			<button type="submit" name="aux" class="btn btn-success" value="aux">Aux.</button>
			<button type="submit" name="stop" class="btn btn-secondary" value="stop">Stop</button>
		</form>
		<br />
	</div>
	<div class="row">
		<div class="col-sm">
			<?php if(ini_get('file_uploads') == 1){?>
				<div style="width: 320px; margin: 0 auto">
					<form action="" method="POST" enctype="multipart/form-data">
						<h4>Upload songs</h4><br>
						<input class="form-control" type="file" name="ficheiro"><br>
						<input style="width:200px" class="form-control" type="submit" value="Upload" id="upload" name="upload">
					</form>
					<p>Max file size: <?php echo str_replace("M"," MB", ini_get('post_max_size'));?><br />Work only with .mp3 files</p>
				</div>
			<?php } ?>
		</div>
		<div class="col-sm">
			<form action="" method="POST">
				<h4>Config</h4>
				<div class="form-group">
					<span title="Frequency"><input type="number" class="form-control" name="frequency" placeholder="Frequency" min="88.0" max="108.0" step='0.1' value="<?php echo $frequency;?>"></span>
				</div>
				<div class="form-group">
					<span title="Station name"><input type="text" class="form-control" name="ps" placeholder="Station name" maxlength="8" value="<?php echo $ps;?>"></span>
				</div>
				<div class="form-group">
					<span title="Radiotext"><input type="text" class="form-control" name="rt" placeholder="Radiotext" maxlength="64" value="<?php echo $rt;?>"></span>
				</div>
					<input class="form-control" type="submit" value="Config" id="config" name="config">
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-sm">

		</div>
	</div>
</div>
</center>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
