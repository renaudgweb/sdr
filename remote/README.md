# web remote pifmrds

### Prerequisites

```
PHP server ( https://www.raspberrypi.org/documentation/remote-access/web-server/apache.md )
git
libsndfile1-dev
```

### Installing


```
sudo apt-get update && sudo apt-get install libsndfile1-dev git -y

mkdir /var/www/html/remote

git clone https://framagit.org/renaudgweb/web-remote-pifmrds.git /var/www/html

mv /var/www/html/web-remote-pifmrds /var/www/html/remote

git clone https://github.com/ChristopheJacquet/PiFmRds.git /var/www/html/remote

mkdir /var/www/html/remote/PiFmRds/audio
```

#### Before using this app, click on the yellow button 'Init.' to run the PiFmRds necessary script. Then you can use the green 'Play' button.

## CREDIT

* https://github.com/ChristopheJacquet/PiFmRds

* https://github.com/RodriguesGabriel/RPiFMBrowserControl

* https://github.com/twbs/bootstrap