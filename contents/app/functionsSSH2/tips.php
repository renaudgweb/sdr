<?php
// Sometimes when a server is busy, or a connection is buggy, the buffer may run dry, and the PHP script stops collecting data from a command output (even though the command hasn't completed yet!). There are a couple of things you could do about that:
ssh2_exec($con, 'ls -al; echo "__COMMAND_FINISHED__"' );
?>

<?php
// Now, in the loop where you keep checking for the buffer, just see if the COMMAND_FINISHED line is coming by. Because then you know you have all the data. To avoid infinite loops, just limit the loop with a timeout of 10 seconds or so:
$time_start = time();
$data       = "";
while (true){
    $data .= fread($stream, 4096);
    if (strpos($data,"__COMMAND_FINISHED__") !== false) {
        echo "okay: command finished\n";
        break;
    }
    if ((time()-$time_start) > 10 ) {
        echo "fail: timeout of 10 seconds has been reached\n";
        break;
    }
}
// In the example above, you'd better set stream_set_blocking to false
?>

<?php
// PHP can send files over ssh
ssh2_scp_send($con, "/tmp/source.dat", "/tmp/dest.dat", 0644);
?>
