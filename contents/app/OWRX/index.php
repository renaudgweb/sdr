<?php

$localIP = $_SERVER['SERVER_ADDR'];

if (($_SERVER["REQUEST_METHOD"] == "POST")){
  if (isset($_POST['on'])){
    $out = exec('sudo systemctl enable openwebrx && sudo systemctl start openwebrx');
    exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/on.wav');
    exec('sudo /home/rengweb/./dit.sh "mise en route du scanne"');
    echo '<div class="alert alert-primary alert-dismissible fade show" role="alert" style="text-align:center">OpenWebRX ON</div>';
    echo '<pre>'.$out.'</pre>';

  } elseif (isset($_POST['off'])){
      $out = exec('sudo systemctl stop openwebrx && sudo systemctl disable openwebrx');
      exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/off.wav');
      exec('sudo /home/rengweb/./dit.sh "arrêt du scanne"');
      echo '<div class="alert alert-secondary alert-dismissible fade show" role="alert" style="text-align:center">OpenWebRX OFF</div>';
      echo '<pre>'.$out.'</pre>';

  } elseif (isset($_POST['halt'])){
      exec('sudo aplay -D hw:0,0 -q /home/rengweb/Musique/bruitages/sdr/halt.wav');
      exec('sudo /home/rengweb/./dit.sh "extinction du système"');
      exec("sudo halt");

  } elseif (isset($_POST['reboot'])){
      exec('sudo aplay -D hw:0,0 -q /home/rengweb/Musique/bruitages/sdr/halt.wav');
      exec('sudo /home/rengweb/./dit.sh "redémarrage du système"');
      exec("sudo reboot");
    }
}


?>

<!DOCTYPE HTML>
<html lang="fr">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/png" href="https://www.renaudgajda.com/favicon.ico">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Stalinist+One&family=Work+Sans:wght@300&display=swap" rel="stylesheet">
  <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../css/smartphone.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<title>OpenWebRX</title>

</head>
<body>
  <div class="container">
    <div class="row">
		    <div class="col-sm">
          <h1 style="text-align:center;">OpenWebRX</h1>

          <form action="" method="POST">
            <div class="form-group">
              <div class="btn-group" role="group">
                  <button type="submit" name="halt" class="btn btn-danger" value="halt" data-toggle="modal" data-target="#staticBackdrop">Shutdown !</button>
              </div>
              <div class="btn-group" role="group">
                  <button type="submit" name="reboot" class="btn btn-warning" value="reboot" data-toggle="modal" data-target="#staticBackdrop">Reboot !</button>
              </div>
            </div>
          </form>

          <form action="" method="POST">
            <div class="form-group">
              <div class="btn-group" role="group">
                <button type="submit" name="on" class="btn btn-success" value="on" data-toggle="modal" data-target="#staticBackdrop">OWRX On</button>
                <button type="submit" name="off" class="btn btn-secondary" value="off" data-toggle="modal" data-target="#staticBackdrop">OWRX Off</button>
              </div>
              <div class="btn-group" role="group">
                <a class="btn btn-primary" href='http://<?php echo $localIP; ?>:8073/' target="_blank">OpenWebRX</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </body>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </html>
