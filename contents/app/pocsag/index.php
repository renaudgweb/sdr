<?php

if (($_SERVER["REQUEST_METHOD"] == "POST")){
  if (isset($_POST['on'])){
    exec("pkill -x rtl_fm && pkill -x multimon-ng && sudo pkill -f 'python3 /home/rengweb/Documents/catprinter/app/stream_data/std_in.py' && pkill -x arecord");
    sleep(2);
    exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/on.wav');
    exec('sudo /home/rengweb/./dit.sh "allumage du décodeur SDR"');
    $out = exec("rtl_fm -f 466.025M -f 466.050M -f 466.075M -f 466.175M -f 466.20625M -f 466.23125M -g 40 -l 70 -s 22.05k - | multimon-ng -a POCSAG512 -a POCSAG1200 -a POCSAG2400 -C FR -e -f alpha -t raw - | sed -e '/Function: [0-9] $/d' -e '/<DEL><DEL><DEL>$/d' | head -c 2m >> /var/www/html/sdr/txt/pocsag.txt &");
    echo '<div class="alert alert-primary alert-dismissible fade show" role="alert" style="text-align:center">RTL-SDR POCSAG Decoder ON</div>';
    echo '<pre>'.$out.'</pre>';

  } elseif (isset($_POST['off'])){
      $out = exec("pkill -x rtl_fm && pkill -x multimon-ng && sudo pkill -f 'python3 /home/rengweb/Documents/catprinter/app/stream_data/std_in.py' && pkill -x arecord");
      exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/off.wav');
      exec('sudo /home/rengweb/./dit.sh "arrêt du décodeur"');
      echo '<div class="alert alert-secondary alert-dismissible fade show" role="alert" style="text-align:center">POCSAG Decoder OFF</div>';
      echo '<pre>'.$out.'</pre>';

  } elseif (isset($_POST['on-aux'])){
      $out = exec("pkill -x rtl_fm && pkill -x multimon-ng && sudo pkill -f 'python3 /home/rengweb/Documents/catprinter/app/stream_data/std_in.py' && pkill -x arecord");
      sleep(2);
      exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/on.wav');
      exec('sudo /home/rengweb/./dit.sh "allumage du décodeur auxiliaire"');
      $out = exec("sudo arecord -fs16_LE -r 44100 -Dplughw:1,0 -c 2 - | multimon-ng -a POCSAG512 -a POCSAG1200 -a POCSAG2400 -C FR -e -f alpha -t raw - | sed -e '/Function: [0-9] $/d' -e '/<DEL><DEL><DEL>$/d' | head -c 2m >> /var/www/html/sdr/txt/pocsag.txt &");
      echo '<div class="alert alert-secondary alert-dismissible fade show" role="alert" style="text-align:center">Aux. POCSAG Decoder ON</div>';
      echo '<pre>'.$out.'</pre>';

  } elseif (isset($_POST['on-print'])){
      exec("pkill -x rtl_fm && pkill -x multimon-ng && sudo pkill -f 'python3 /home/rengweb/Documents/catprinter/app/stream_data/std_in.py' && pkill -x arecord");
      sleep(2);
      exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/on.wav');
      exec('sudo /home/rengweb/./dit.sh "allumage du décodeur SDR pour impression thermique"');
      $out = exec("rtl_fm -f 466.025M -f 466.050M -f 466.075M -f 466.175M -f 466.20625M -f 466.23125M -g 40 -l 70 -s 22.05k - | multimon-ng -a POCSAG512 -a POCSAG1200 -a POCSAG2400 -C FR -e -f alpha -t raw - | sed -e '/Function: [0-9] $/d' -e '/<DEL><DEL><DEL>$/d' | python3 /home/rengweb/Documents/catprinter/app/stream_data/std_in.py &");
      echo '<div class="alert alert-primary alert-dismissible fade show" role="alert" style="text-align:center">POCSAG Live Catprint ON</div>';
      echo '<pre>'.$out.'</pre>';

  } elseif (isset($_POST['on-aux-print'])){
      $out = exec("pkill -x rtl_fm && pkill -x multimon-ng && sudo pkill -f 'python3 /home/rengweb/Documents/catprinter/app/stream_data/std_in.py' && pkill -x arecord");
      sleep(2);
      exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/on.wav');
      exec('sudo /home/rengweb/./dit.sh "allumage du décodeur auxiliaire pour impression thermique"');
      $out = exec("sudo arecord -fs16_LE -r 44100 -Dplughw:1,0 -c 2 - | multimon-ng -a POCSAG512 -a POCSAG1200 -a POCSAG2400 -C FR -e -f alpha -t raw - | sed -e '/Function: [0-9] $/d' -e '/<DEL><DEL><DEL>$/d' | python3 /home/rengweb/Documents/catprinter/app/stream_data/std_in.py &");
      echo '<div class="alert alert-secondary alert-dismissible fade show" role="alert" style="text-align:center">Aux. POCSAG Live Catprint ON</div>';
      echo '<pre>'.$out.'</pre>';

  } elseif (isset($_POST['feed'])){
      exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/feed.wav');
      exec('sudo /home/rengweb/./dit.sh "déroulage du papier"');
      $out = exec("curl --location --request POST --form 'feed=\"100\"' 'localhost:5000'");
      echo '<div class="alert alert-primary alert-dismissible fade show" role="alert" style="text-align:center">Feed !</div>';
      echo '<pre>'.$out.'</pre>';

  } elseif (isset($_POST['halt'])){
      exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/halt.wav');
      exec('sudo /home/rengweb/./dit.sh "extinction du système
"');
      exec("sudo halt");

  } elseif (isset($_POST['reboot'])){
      exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/halt.wav');
      exec('sudo /home/rengweb/./dit.sh "redémarrage du système"');
      exec("sudo reboot");

  } elseif (isset($_POST['tatootransmit']) && isset($_POST['messagetatoo']) && !empty($_POST['messagetatoo'])){
      exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/transmit.wav');
      exec('sudo /home/rengweb/./dit.sh "transmission en cours"');
      $message = htmlspecialchars($_POST['messagetatoo']);
      $out = escapeshellcmd(exec('sudo /home/rengweb/./send_pocsag_tatoo.sh '.$message.''));
      echo '<div class="alert alert-success alert-dismissible fade show" role="alert" style="text-align:center">TATOO Transmission sent</div>';
      echo '<pre>'.$out.'</pre>';

  } elseif (isset($_POST['pocsagtransmit']) && isset($_POST['messagepocsag']) && !empty($_POST['messagepocsag']) && isset($_POST['capcode']) && !empty($_POST['capcode'])){
      exec('sudo aplay -Dhw:0,0 -q /home/rengweb/Musique/bruitages/sdr/transmit.wav');
      exec('sudo /home/rengweb/./dit.sh "transmission en cours"');
      $message = htmlspecialchars($_POST['messagepocsag']);
      $capcode = htmlspecialchars($_POST['capcode']);
      $out = escapeshellcmd(exec('sudo /home/rengweb/./send_pocsag.sh '.$capcode. ' '.$message.''));
      echo '<div class="alert alert-success alert-dismissible fade show" role="alert" style="text-align:center">POCSAG Transmission sent</div>';
      echo '<pre>'.$out.'</pre>';
  }
}

?>
<!DOCTYPE HTML>
<html lang="fr">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" type="image/png" href="https://www.renaudgajda.com/favicon.ico">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Stalinist+One&family=Work+Sans:wght@300&display=swap" rel="stylesheet">
  <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../../css/smartphone.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<title>POCSAG</title>

</head>
<body>
  <div class="container">
    <div class="row">
        <div class="col-sm">
          <h1 class="text-center text-gray-400">POCSAG</h1>

          <form action="" method="POST">
            <div class="form-group">
              <div class="btn-group" role="group">
                  <button type="submit" name="halt" class="btn btn-danger" value="halt" data-toggle="modal" data-target="#staticBackdrop">Shutdown !</button>
              </div>
              <div class="btn-group" role="group">
                  <button type="submit" name="reboot" class="btn btn-warning" value="reboot" data-toggle="modal" data-target="#staticBackdrop">Reboot !</button>
              </div>
            </div>
          </form>

          <form action="" method="POST">
            <div class="form-group">
              <div class="btn-group" role="group">
                <button type="submit" name="off" class="btn btn-secondary" value="off" data-toggle="modal" data-target="#staticBackdrop">Decoder Off</button>
              </div>
            </div>
          </form>

          <form action="" method="POST">
            <div class="form-group">
              <div class="btn-group" role="group">
                <button type="submit" name="on" class="btn btn-success" value="on" data-toggle="modal" data-target="#staticBackdrop">RTL-SDR On</button>
              </div>
              <div class="btn-group" role="group">
                <button type="submit" name="on-aux" class="btn btn-success" value="on-aux" data-toggle="modal" data-target="#staticBackdrop">Aux. Dec. On</button>
              </div>
              <div class="btn-group" role="group">
                <a class='btn btn-info' data-role='button' data-inline='true' href='../../../txt/pocsag.txt' target="_blank">Pagers TXT</a>
              </div>
            </div>
          </form>

          <form action="" method="POST">
            <div class="form-group">
              <div class="btn-group" role="group">
                <button type="submit" name="on-print" class="btn btn-success" value="on-print" data-toggle="modal" data-target="#staticBackdrop">SDR print On</button>
              </div>
              <div class="btn-group" role="group">
                <button type="submit" name="on-aux-print" class="btn btn-success" value="on-aux-print" data-toggle="modal" data-target="#staticBackdrop">Aux. print On</button>
              </div>
              <div class="btn-group" role="group">
                <button type="submit" name="feed" class="btn btn-primary" value="feed" data-toggle="modal" data-target="#staticBackdrop">Feed paper</button>
              </div>
            </div>
          </form>

          <form action="" method="POST">
            <div class="form-group">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <button type="submit" name="tatootransmit" class="btn btn-danger" value="tatootransmit" data-toggle="modal" data-target="#staticBackdrop" id="button-addon1">Transmit to TATOO</button>
                </div>
                <input class="form-control form-control-lg" name="messagetatoo" aria-describedby="button-addon1" placeholder="numeric message" maxlength="10" value="messagetatoo" required>
              </div>
            </div>
          </form>

          <form action="" method="POST">
            <div class="form-group">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <button type="submit" name="pocsagtransmit" class="btn btn-danger" value="pocsagtransmit" data-toggle="modal" data-target="#staticBackdrop" id="button-addon2">Transmit POCSAG</button>
                </div>
                <input type="number" class="form-control form-control-lg" name="capcode" aria-describedby="button-addon2" placeholder="CAP Code" maxlength="7" value="capcode" required>
                <input class="form-control form-control-lg" name="messagepocsag" aria-describedby="button-addon2" placeholder="alphanumeric message" maxlength="160" value="messagepocsag" required>
              </div>
            </div>
          </form>

      </div>
    </div>
  </div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>
