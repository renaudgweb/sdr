<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0, shrink-to-fit=no, user-scalable=no">
    <link rel="icon" type="image/png" href="#">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Stalinist+One&family=Work+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="contents/css/smartphone.css">
    <title>SDR</title>
</head>

<body>
  <main>
    <header class='text-center'>
      <h1 class="text-gray-400">Software Defined Radio</h1>
    </header>
    <section>
      <nav>
        <a class='text-white' href='contents/app/APRS' target="_blank">APRS</a>
        <a class='text-white' href='contents/app/ACARS' target="_blank">ACARS</a>
        <a class='text-white' href='contents/app/ISM433' target="_blank">ISM-433</a>
        <a class='text-white' href='contents/app/pocsag' target="_blank">POCSAG</a>
        <a class='text-white' href='remote/index.php' target="_blank">FM-RDS</a>
        <a class='text-white' href='contents/app/OWRX' target="_blank">OpenWebRX</a>
      </nav>

      <div class="text-center">
        <img class="logoHam w-32 h-auto mx-auto" src="contents/images/radio.svg" onerror="this.src='contents/images/radio.png'">
      </div>
    </section>

    <footer class='text-center'>
      <h4 class="text-gray-400">&copy; <script>document.write((new Date()).getFullYear());</script> - <i><a href='#/' target='_blank'>Renaud G.</a></i></h4>
    </footer>

  </main>
</body>
</html>
